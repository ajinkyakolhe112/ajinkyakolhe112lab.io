---
layout: post
title:  "How to get good at Programming."
description: Learned syntax of an language and looking to improve your coding skills? Programming is not same as syntax.
date:   2017-11-05 14:10:51 +0800
categories: ['blog', 'programming']
tags: ['Programming']
---
## Learn Programming

Despite what many programming tutorial websites have you believe, you can't learn programming in 21 hours. You can learn syntax in 21 hours, but programming is all about problem solving. And you develop problem solving skills only by doing problem solving. Check out this article for more in-depth information, [Learn Programming by Peter Norvig](http://norvig.com/21-days.html). **Learning syntax is the easy part.** There are many books and websites which can help you learn syntax.

### Learning the Syntax (Easy part)

For beginners, you should choose from following. Combined together they have more than 50% of the market share.
1. Java.
  - Resource [Head First Java](http://shop.oreilly.com/product/9780596009205.do)
2. Python.
  - Resource [Head First Python](shop.oreilly.com/product/0636920003434.do)
3. C.
  - Resource [Head First C](shop.oreilly.com/product/0636920015482.do)

I have kept C at the bottom because for beginners it might be more difficult to pick up. At advanced level to understand actual implementation details of data structures and memory, it is the best language. But if you want to focus more on problem solving, it is better to choose a language like Python or Java which hides the underlying complexity.
I recommend all books in "Head First Series" because they focus on concepts and practical coding skills both.

### Improve your Programming(Hard Part)

Programming is application oriented. Programming is problem solving. After you learn syntax, you can only improve programming by doing problem solving.

1. **Solve Problems. DO NOT COPY PASTE**
    - Develop your own logic.
    - Developing your logic helps you solve new problems. And that is the most important part.
2. **Write Maintainable Code**
    - Learn to write clean code. Check out [Clean Code](https://www.amazon.com/Clean-Code-Handbook-Software.../dp/0132350882),[Beautiful Code](hop.oreilly.com/product/9780596510046.do)
3. **Test the Code**
    - Use Test Driven Development.
    - Do unit testing and mock testing, no matter how small the code.
    - Use logging
4. **Setup Development Environment**
    - Use IDE effectively
    - Use Version Control
    - Use Continuous Integration for testing coverage, code quality metric etc
