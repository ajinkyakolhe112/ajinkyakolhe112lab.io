---
layout: training
title: Deep Learning For Beginners
description: Deep Learning Training for Beginners
---

Learn to use Keras and Tensorflow to build deep learning models on real-world data.

The workshop is meant to introduce you to the concepts of deep learning and develop a solid base to build deeper knowledge in the field. You will learn to:

- Fundamentals of deep learning theory
- How to approach and solve a problem with deep learning
- Build and train a deep fully connected model with Keras
- Build and train a Convolution Neural Net with Keras on a CPU/GPU machine
- Build and train a Recurrent Neural Net with Keras on a CPU/GPU machine
- Application to Image processing/Text processing

## Curriculum

1. **Setting up the Environment**
    - Setting Up Environment for Deep Learning
    - Keras, Tensorflow, Jupyter etc
    - Quick overview of Python
2. **Theoretical Foundation of Deep Learning**
    - Overview of Deep Learning
    - Deep Learning vs Machine Learning
    - Phases of Deep Learning Project
3. **Understanding Neural Networks**
    - Understanding Neural Network
    - How neural networks learn
    - Architecture of Neural Networks
    - Convolution Neural Networks
    - Recurrent Neural Networks
4. **Convolution Neural Networks for Image Processing**
    - Theory of Convolution Layers
    - Using 1D CNN, 2D CNN on MNIST
    - Using CNN for image classification
5. **Recurrent Neural Networks for Natural Language Processing**
    - Theory Recurrent Layers
    - Using RNN, LSTM, GRU for Sentiment analysis
    - Word2Vec, Text2Text, Glove Embedding
    - Understanding Vanishing Gradient
6. **Using Keras**
    - Using Dropout, Flatten, Merge
    - Optimizers
    - Activation Functions
    - Finding best model parameters
    - Saving models
    - Deploying models
