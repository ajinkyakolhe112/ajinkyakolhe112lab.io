---
layout: training
title: Learn Python Programing
description: Python Training
---

Python is an ideal language for a beginner or experienced developer. Its ease of use and extensive libraries (100k packages till now) make Python, **"A Must Know Language in 21st Century"** especially with trend of Data Science and IoT.

## Curriculum

Learning language syntax is not same as software development. Learn both, Python Syntax and Software Development Skills in one workshop.

1. **Introduction to Python**
  - The Python Philosophy
  - Fundamentals
  - Using python interpreter
  - Running Simple python programs
  - Variables
  - A detailed treatment of the string facilities of Python
2. **Data structures & Control flow**
  - Lists
  - Tuples
  - Dictionaries
  - Conditional Statements
  - Loops
3. **Functions, File Handling & Algorithms**
  - Functions
  - Variables revisited
  - Recursive functions
  - File handling, I/O
  - Writing algorithms using data structures, control flow & functions
  - Debugging
4. **Object Oriented Programming**
  - Overview of Object-oriented programming
  - Constructor
  - Objects, Instances and classes
  - Encapsulation
  - Polymorphism
  - Inheritance and the object hierarchy
  - Exception Handling
5. **Learning Project Development**
  - Using git
  - Project development on github
  - Static code analysis
  - Setting up development environment
  - Test Driven Development
  - Logging
  - Miscellaneous code reading & understanding skills
