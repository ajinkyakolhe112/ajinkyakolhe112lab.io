---
layout: training
title: Artificial Intelligence Training Portfolio
description: Training Portfolio for Python, Machine Learning and Deep Learning.
---
# Training Portfolio

I conduct following trainings which emphasize on developing hands-skill which can be used to develop practical applications. If a beginner starts with learning Linear Algebra, Probability and all other pre-requisite as suggested by many, it becomes too difficult to actually pick up the knowledge. I will focus on developing practical skills first, which makes it easier to develop in depth knowledge but other way around is very difficult.


## 1. [Python Programming]({{site.baseurl}}/training/Python)
Python is an ideal language for a beginner or experienced developer. Ease of use and extensive available libraries make Python, **"A Must Know Language in 21st Century"** especially with trend of Data Science and IoT.

Learn Python Programming to be used in Application Development, Data Science, Automation or any other area. Practical project development on Github included in the course.  

## 2. [Machine Learning for Beginners in Python]({{site.baseurl}}/training/Machine-Learning-Beginners)
R and Python, are top two choices for implementing Machine Learning. Choosing Python as it makes it easier for beginners and people who want to shift to data science. R is better choice for people with statistical background.

Learn to use Pandas, Matplotlib, Numpy and Scikit-learn to build predictive models from a real-world data. Learn scikit-learn and its feature in depth and apply it to different datasets.  

## 3. [Deep Learning for Beginners using Tensorflow & Keras]({{site.baseurl}}/training/Deep-Learning-Beginners)
Tensorflow is the top deep learning library available. Easy to use and easy to scale making it top choice of researchers and developers. Learn to develop neural networks using Tensorflow and Keras.

Learn to use Keras, Tensorflow, Scikit-learn to build deep learning models from a real-world data. Do projects in computer vision and natural language processing. Learn when to use machine learning and when to use deep learning. 
